// English
const lang = {
    contact: 'Contact',
    social: 'Social',
    born: 'Born',
    bornIn: 'in',
    experience: 'Experience',
    education: 'Education',
    skills: 'Skills',
    technologies: 'Technologies',
    languages: 'Languages',
    about: 'About me',
    summary: 'Summary',
    certifications: 'Certifications',
    projects: 'Projects',
    contributions: 'Contributions'
};
export default lang;
